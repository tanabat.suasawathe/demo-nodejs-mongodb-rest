FROM node:16-alpine3.11
# Create app directory

# Install app dependencies
COPY package*.json ./
RUN npm install
# Copy app source code
COPY . .

#Expose port and start application
CMD [ "npm", "start" ]
